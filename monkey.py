""" Distributed under GNU Affero GPL license.
    
    Monkey Counter v1.0.1 20/12/2021.

    Copyright (C) 2019-2020  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *

#--- FILE-RELATED FUNCTIONS ---

def get_ids():
    #This function read the .txt file with all the ids
    
    with open(ids_file,'r') as file:
        ids = file.readlines()
    return ids

def add_id(channel_id):
    #This function add an id to the .txt file and return True if success

    with open(ids_file,'r') as file:
        ids = file.read()
        
    if str(channel_id) not in ids:
        ids += "\n"+str(channel_id)

        with open(ids_file,'w') as file:
            file.write(ids)
            
        return True

    return False    

#--- CHANNEL-RELATED FUNCTIONS ---
    
def count():
    #This function count the subscribers and produce a summary message
    
    ids = get_ids()
    message = "Subscribers counted:\n\n"
    total_count = 0

    for channel in ids:
        
        try:
            channel_info = bot.getChat(channel,100)
            channel_fields = list(channel_info.to_dict())
            members = bot.getChatMembersCount(channel,100)

            #The bot tries to give every channel a name with a link. For public channel, it's simple the username
            #For private channel, it's the invite link formatted with the title
            #The HTML escape is used to avoid HTML error caused by bizarre channel titles 
            
            if ('username' in channel_fields):
                message += '@'+channel_info.username
            else:
                message += "<a href='"+channel_info.invite_link+"'>"+html.escape(channel_info.title)+"</a>"

            message+= " - "+str(members)+"\n"
            total_count += members

        except:
            None

    message += "\nTotal count: "+str(total_count)
    return message

def send_results():
    #This function sends the stats message to the channel
    
    bot.sendMessage(
        chat_id = stat_channel_id,
        text = count(),
        parse_mode = 'HTML',
        disable_web_page_preview = True,
        reply_markup = InlineKeyboardMarkup(
            [[
            InlineKeyboardButton(
                text = 'Source code',
                url = source_url
                )
             ]]
            )
        )
