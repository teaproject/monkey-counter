""" Distributed under GNU Affero GPL license.

    Configuration file for Monkey Counter v1.0.1 20/12/2021.

    Copyright (C) 2019-2020  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#--- LIBRARIES IMPORT ---
import html
import logging
from pathlib import Path
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, UpdateFilter
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import time

#--- VARIABLES DEFINITION ---
api_token = '0'
owner_id = 0
ids_file = 'ids.txt' #Where the id list file is
stat_channel_id = 0 #it's the channel where the stats are sent
source_url = 'https://gitlab.com/tea-project/monkey-counter'
do_polling = True #if true, the bot is usable by users, is constantly active and send the stats once a day (at the polling_send_hour). If false, send the stats every time the file is executed
polling_send_hours = [12] #If polling, at this hours every day send the stats to the stat channel

updater = Updater(token=api_token, use_context=True)
dispatcher = updater.dispatcher
bot = updater.dispatcher.bot

#--- LOGGING CONFIGURATION ---
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

#--- NEEDED FILES CHECK ---
id_list = Path(ids_file)
if not id_list.is_file():
    #The ids_file was not created yet
    open(ids_file,'w')
