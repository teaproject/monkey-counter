""" Distributed under GNU Affero GPL license.

    Monkey Counter v1.0.1 20/12/2021.

    Copyright (C) 2019-2020  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *
import monkey
import custom_filters

#--- USER'S FUNCTIONS ---

def start_callback(update, context):
    #/start, /help, /info commands. Gives general info about the bot
    
    message = """Welcome \U0001F60A.\n
*Monkey Counter* is a software that will help you take track of all your channels evolution over time.\n
Given a list of channels I will go every day to see how many subscribers they have and I will write all down in a specific personal channel.\n
*Do you want to add another channel to the supervision list?* Just forward me a message from there and, if the channel is private, add me to it!"""
    
    update.message.reply_text(
        text = message,
        parse_mode=telegram.ParseMode.MARKDOWN,
        reply_markup = InlineKeyboardMarkup(
            [[
        InlineKeyboardButton(
            text = 'Source code',
            url = source_url
                )
             ]]
            )
        )

def add_channel(update, context):
    #When receive a forwarded message that is from a channel, try to add the channel to the ids list

    channel_id = update.message.forward_from_chat.id
    add_success = monkey.add_id(channel_id)
    
    if add_success:
        update.message.reply_text(text = "I added the channel to the list with success \U0001F60A!")
    else:
        update.message.reply_text(text = "I couln't add the channel to the list. Maybe is already there?") 
        
# --- HANDLING AND POLLING ---

filter_forwarded_from_channel = custom_filters.forwarded_from_channel()

start_handler = CommandHandler(
    command = ['start','help','info'],
    callback = start_callback,
    filters = (Filters.private)
    )


#The forwarded_from_channel_handler accepts all and only the messagges that are forwarded from a public channel or a channel the bot is subscribed to (the bot has to be administrator of a channel to join it)
forwarded_from_channel_handler = MessageHandler(
    filters = (Filters.private & filter_forwarded_from_channel & Filters.user(user_id=owner_id)),
    callback = add_channel
    )


dispatcher.add_handler(start_handler)
dispatcher.add_handler(forwarded_from_channel_handler)

if do_polling:
    #If the bot is setted to poll for user's text, it does
    
    updater.start_polling()
    action = 0
    
    #The value action is used to ensure certain action are done only once for hour
    #During poling is important that the bot, at least one time a day, send the stats to the specific channel. This is done during the polling_send_hours specified in the config.py file
        
    while True:
        
        if int(time.strftime('%H')) in polling_send_hours and action == 0:
            monkey.send_results()
            action = time.strftime('%H')

        elif action != time.strftime('%H'):
            #This sistem will reset the value action to zero every time the hour change or, in other words, every hour 
            action = 0

        time.sleep(5)

else:
    #The bot is not setted to poll. This way it run only when needed
    monkey.send_results() 
