""" Distributed under GNU Affero GPL license.
    

    Custom filters for Monkey Counter v1.0.1 20/12/2021.

    Copyright (C) 2019-2020  Davide Leone

    You can contact me at leonedavide[at]protonmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *

class forwarded_from_channel(UpdateFilter):
    
    def filter(self, message):
        
        if 'forward_from_chat' in list(message.to_dict()):
            
            author = message.forward_from_chat.id
            try:
                author_info = bot.getChat(author,10)
                if author_info.type == 'channel':
                    return True
            except:
                None

        return False
